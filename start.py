import cv2
import random
import matplotlib.pyplot as plt
import sys

import tensorflow as tf
# Disable Tensorflow warnings
tf.logging.set_verbosity(tf.logging.ERROR)

# import inference code
from car_damage import Inference

from os import rename

filename = sys.argv[1]

# Enable plotting in notebook
#%%matplotlib inline


# Set up file paths
random_file = random.randint(300, 900)
weights_path = "./mask_rcnn_car_damage_0030.h5" # pre-trained model weights path
#image_path = "dataset/{0:04}.JPEG".format(random_file) # Input Image path
image_path = "./uploads/" + filename         #이 이미지가 서버에 업로드 됨

# Inference on new image
result = Inference(weights_path, image_path)

# show the results
img = cv2.imread(image_path) # raw input image
img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)

fig = plt.figure(figsize=(15, 10))
# 결과 분석
print('damage: {0:.2f}%'.format(result[1]))


# plt.subplot(1, 2, 1)
# plt.imshow(img)

# res = plt.subplot(1, 2, 2)
# res = plt.subplot(1, 2, 1)
# plt.imshow(result[0])
# plt.show()

# save the image
plt.imsave('./output.jpg', result[0])
rename('./output.jpg', filename)
rename(filename, './output/' + filename)

# %%
