import { PythonShell } from 'python-shell';

require('dotenv').config();

const options = {
  mode: 'text',
  pythonPath: process.env.PYTHON_PATH,
  pythonOptions: ['-u'],
}
export const pyShell = (
  script,
  args,
) => new Promise((resolve, reject) => PythonShell.run(
  script,
  { args, ...options },
  (err, results) => {
    if(err) reject(err);
    resolve(results);
  }
));