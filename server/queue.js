import Queue from 'bee-queue';
import redis from 'redis';
import env from 'dotenv';
import { pyShell } from './bash';


env.config();
// create redis server
const client = redis.createClient(process.env.REDIS_URL, { password: process.env.REDIS_PWD });

client.on('error', err => console.error(`Error: ${err}`))

const queue = new Queue('damage', {
  redis: client,
});

queue.process(async (job) => {
  try{
    job.reportProgress(10);
    console.log(`Processing job ${job.data.filename}`);
    const data = await pyShell('start.py', [job.data.filename]);
    const str_damaged_rate = data[57];
    const num = str_damaged_rate.replace(/[^0-9]/g,'');
    const damaged_rate = num / 100;

    // console.log('Damage: ', str_damaged_rate);
    // console.log('Damage percentage: ', damaged_rate);
    return damaged_rate;
  } catch(err) {
    console.error('Error:', err);
  }
});

export default queue;