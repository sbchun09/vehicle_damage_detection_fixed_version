import SocketIO from 'socket.io';
import SocketUploader from 'socketio-file-upload';
import queue from '../queue';
import path from 'path';

const io = SocketIO();

io.on('connection', socket => {
  console.log(`socket id[${socket.id}] connected`);
  const uploader = new SocketUploader();

  uploader.dir = path.join(__dirname, '../../uploads');
  uploader.listen(socket);

  uploader.on('start', e => {
    if (/\.exe$/.test(e.file.name)) {
        uploader.abort(e.file.id, socket);
    }
  });
  uploader.on('saved', e => {
    const uploaded = e.file;
    const filetype = uploaded.name.substring(uploaded.name.lastIndexOf(".") + 1);
    const filename = uploaded.base + '.' + filetype;
    e.file.clientDetail.filename = filename;

  });
  uploader.on('error', e => {
    console.log('Error from uploader', e);
  });
  socket.on('Detection start', async filename => {
    if(!filename){
      return;
    }

    const job = await queue.createJob({
      filename,
    }).save();
    job.on('succeeded', (damaged_rate) => {
      socket.emit('Detection succeeded', {
        filename,
        damaged_rate
      });
    });
    job.on('progress', progress => {
      socket.emit('Detection progress', progress);
    });
    socket.emit('Detection start', { filename });
  })
});
export default io;
