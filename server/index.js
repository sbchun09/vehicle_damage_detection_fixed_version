import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import cors from 'cors';
import env from 'dotenv';
import api from './api';
import io from './socket';
import ejs from 'ejs';
import path from 'path';
import { router } from 'socketio-file-upload';

env.config();

console.log(process.env.PORT);
const port = process.env.PORT || 5000;

const app = express();
const server = http.createServer(app).listen(port, () => {
  console.log(`app is listening to port ${port}`);
});
// use socket.io
app.io = io;
app.io.attach(server);
app.use(router);

// Middlewares
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Serve static files
app.use(express.static('static'));
app.use(express.static('output'));


// Set View Engine
app.set('views', path.join(__dirname, '../views'));
app.set('view engine', 'ejs');
app.engine('html', ejs.renderFile);

// app.use('/api', api);
app.get('/', (req, res) => res.render('index'));