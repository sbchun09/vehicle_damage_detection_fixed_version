import express from 'express';
import multer from 'multer';
import path from 'path';
import fs from 'fs';
import queue from './queue';
import io from './socket';

const router = express.Router();
// multer setting
const storage_image = multer.diskStorage({
  destination: function(req, file, cb) {
      cb(null, "image/");
  },
  filename: function(req, file, cb) {
      cb(null, file.originalname);
  }
});
const upload_image = multer({storage: storage_image});

const storage_video = multer.diskStorage({
  destination: function(req, file, cb) {
      cb(null, "video/");
  },
  filename: function(req, file, cb) {
      cb(null, file.originalname);
  }
});
const upload_video = multer({storage: storage_video});

// when uploading video => enque the video
router.post('/video', upload_video.single('file'), (req, res, next) => {
  console.log("[I] Upload video request");

  const uploaded = req.file;
  if(!uploaded){
    res.sendStatus(404);
  }
  const filename = uploaded.originalname;
  // const filePath = path.join(__dirname, uploaded.path);
  // const filetype = req.file.originalname.substring(req.file.originalname.lastIndexOf(".") + 1);

  const { socketId } = req.body;
  // enque
  const job = queue.createJob({
    filename,
    type: 'video',
  }).save();
  io.to(socketId).emit('uploaded', { filename, type: 'video' });
  // enqueue the video received
  // fs.appendFile("queue/queue.txt", req.file.originalname + " video false\n", (err) => {
  //     if (err) return res.status(400).send("Error enqueuing a video: " + err.toString());
  //     return res.status(200).send(req.file.originalname);
  // });
})
router.get('/output/:filename', (req, res, next) => {
  const filename = req.params.filename.substring(0, req.params.filename.lastIndexOf('.'));
  const filetype = req.params.filename.substring(req.params.filename.lastIndexOf('.')+1);
  const output_filepath = path.join(__dirname, "../output/" + filename + "_output.mp4");
  var stat = fs.statSync(output_filepath);
  res.setHeader('Content-Length', stat.size);
  res.setHeader('Content-Type', 'video/mp4');
  res.setHeader('Content-Disposition', 'attachment; filename=' + filename + '_output.mp4');
  var file = fs.readFile(output_filepath, 'binary', (err, data) => {
    if (err) {
        console.log(err);
        return res.status(400).send("Error while reading output file: " + err.toString());
    }
      res.write(data, 'binary');
      res.end(null, 'binary');
  });
});

export default router;